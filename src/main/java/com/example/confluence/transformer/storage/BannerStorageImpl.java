package com.example.confluence.transformer.storage;

import com.atlassian.confluence.labels.Label;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Provides a concrete implementation of the {@link BannerStorage} component
 * interface.
 */
public class BannerStorageImpl implements BannerStorage
{
    private static final Logger log = LoggerFactory.getLogger(BannerStorageImpl.class);
    private static final String BANNER_STORAGE_KEY = "banners";

    private final PluginSettings pluginSettings;

    /**
     * Constructs a new instance of {@link BannerStorageImpl}. This constructor
     * is called by Confluence when the plugin is initialised.
     *
     * @param pluginSettingsFactory The {@link PluginSettingsFactory} component
     *                              provided by the Shared Access Layer ("SAL")
     *                              plugin. Confluence will automatically find
     *                              this component and set it as the
     *                              constructor parameter for your component.
     */
    public BannerStorageImpl(PluginSettingsFactory pluginSettingsFactory)
    {
        // Use the SAL pluginSettingsFactory to create a new instance of
        // the PluginSettings object. We can use this object to save simple
        // Key/Value pairs in the Confluence database. The value objects must
        // be serializable Java objects or primitive types.
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addBanner(Label label, BannerType bannerType, String bannerTitle, String bannerText)
    {
        log.debug(String.format("Adding new Banner (%s) for Label (%s)", bannerText, label.getDisplayTitle()));
        Map<Long, Banner> banners = getBanners();
        banners.put(label.getId(), new Banner(label.getId(), bannerType, bannerTitle, bannerText));
        saveBanners(banners);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Banner> getConfiguredBanners()
    {
        // Return a collection that cannot be modified - this prevents other code
        // from sneakily doing things that bypass the public Add and Remove
        // methods on the BannerStorage interface.
        return Collections.unmodifiableCollection(getBanners().values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeBanner(Label label)
    {
        log.debug(String.format("Removing the banner associated with Label %s", label.getDisplayTitle()));
        Map<Long, Banner> banners = getBanners();
        banners.remove(label.getId());
        saveBanners(banners);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Banner getBannerForLabel(Label label)
    {
        return getBanners().get(label.getId());
    }

    /**
     * A private method to load the banners from the database. Because we are
     * storing the banners in a {@link Map} as a single Key/Value pair in the
     * database, we must load the entire map into memory from the database in
     * order to add or remove a single entry, and then save the entire map
     * back to the database again.
     *
     * Note that this approach to database persistence works fine for small
     * sets of data, but would not scale well to thousands or millions of
     * entries.
     */
    @SuppressWarnings("unchecked") // This should never happen.
    private Map<Long, Banner> getBanners()
    {
        Object value = pluginSettings.get(BANNER_STORAGE_KEY);
        if (value == null)
            // Initialise the Map if it doesn't yet exist in the database.
            return Maps.newHashMap();

        return (Map<Long, Banner>)value;
    }

    /**
     * A private method to save the configured banners back to the database.
     */
    private void saveBanners(Map<Long, Banner> banners)
    {
        pluginSettings.put(BANNER_STORAGE_KEY, banners);
    }
}
