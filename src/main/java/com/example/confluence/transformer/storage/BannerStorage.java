package com.example.confluence.transformer.storage;

import com.atlassian.confluence.labels.Label;

import java.util.Collection;

/**
 * Defines the operations available on the bannerStorage component (see the
 * atlassian-plugin.xml). Responsible for saving and loading instances of
 * {@link Banner} to and from the Confluence database.
 */
public interface BannerStorage
{
    /**
     * Creates a new instance of {@link Banner} to the database, using the
     * provided information. The newly-created {@link Banner} can be
     * subsequently retrieved using the
     * {@link #getBannerForLabel(com.atlassian.confluence.labels.Label)} method.
     * <p/>
     * If a {@link Banner} already exists with the same {@link Label}, the
     * existing {@link Banner} will be overwritten and replaced by this one.
     *
     * @param label The {@link Label} to which this banner should apply.
     * @param type  The {@link BannerType} of the new banner.
     * @param title The optional title for the banner. Specify null or ""
     *              to create a banner with no title.
     * @param text  The body text of the new banner.
     */
    public void addBanner(Label label, BannerType type, String title, String text);

    /**
     * Returns all currently configured instances of {@link Banner}. If there
     * are currently no banners configured, an empty collection will be
     * returned.
     *
     * @return An un-ordered collection of all configured Banner instances.
     */
    public Collection<Banner> getConfiguredBanners();

    /**
     * Deletes the {@link Banner} associated with the specified {@link Label},
     * if one exists. If none exists, this method takes no action.
     *
     * @param label The corresponding {@link Label} object.
     */
    public void removeBanner(Label label);

    /**
     * Retrieves the {@link Banner} associated with the specified {@link Label},
     * if one exists. If none exists, this method will return {@code null}.
     *
     * @param label The corresponding {@link Label} object.
     * @return The {@link Banner} associared with the specified {@link Label},
     *         or {@code null} if no banner exists.
     */
    public Banner getBannerForLabel(Label label);
}
