package com.example.confluence.transformer.storage;

/**
 * This enumeration is used to persist the 'kind' of banner being saved to the database.
 * See {@link BannerStorage}
 */
public enum BannerType
{
    /**
     * Indicates that the banner should be created using the {info} macro.
     */
    INFO,

    /**
     * Indicates that the banner should be created using the {warning} macro.
     */
    WARNING,

    /**
     * Indicates that the banner should be created using the {tip} macro.
     */
    TIP,

    /**
     * Indicates that the banner should be created using the {note} macro.
     */
    NOTE
}
