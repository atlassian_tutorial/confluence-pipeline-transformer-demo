package com.example.confluence.transformer.storage;

/**
 * Represents an individual Banner that has been configured and saved to the
 * database.
 * See {@link BannerStorage}
 */
public class Banner
{
    private long labelId;
    private BannerType bannerType;
    private String bannerTitle;
    private String bannerText;


    /**
     * A no-args constructor. This is necessary in order for the serialisation
     * of this class to and from the database to function correctly.
     */
    public Banner()
    {
        // No-Op.
    }

    /**
     * A convenience constructor so that the {@Link BannerStorageImpl} class
     * can easily create new, pre-configured instances of {@link Banner}
     *
     * @param labelId The unique ID of the label that this banner applies to.
     * @param type    The kind of banner (eg. info/note/tip)
     * @param title   The optional title of the banner
     * @param text    The body text contained within the banner
     */
    Banner(long labelId, BannerType type, String title, String text)
    {
        this.labelId = labelId;
        this.bannerText = text;
        this.bannerTitle = title;
        this.bannerType = type;
    }

    public void setLabelId(long labelId)
    {
        this.labelId = labelId;
    }

    public long getLabelId()
    {
        return labelId;
    }

    public void setBannerText(String bannerText)
    {
        this.bannerText = bannerText;
    }


    public String getBannerText()
    {
        return bannerText;
    }

    public void setBannerType(BannerType bannerType)
    {
        this.bannerType = bannerType;
    }


    public BannerType getBannerType()
    {
        return bannerType;
    }

    public void setBannerTitle(String bannerTitle)
    {
        this.bannerTitle = bannerTitle;
    }


    public String getBannerTitle()
    {
        return bannerTitle;
    }
}
