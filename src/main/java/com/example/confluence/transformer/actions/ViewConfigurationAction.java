package com.example.confluence.transformer.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.example.confluence.transformer.storage.Banner;
import com.example.confluence.transformer.storage.BannerStorage;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.opensymphony.xwork.Action;

import java.util.Collection;

/**
 * The action controller class for the "View" action of the plugin. It is a
 * general convention to name all action classed with a suffix of "Action". It
 * is also strongly recommended to have the action extend the
 * {@link ConfluenceActionSupport} class.
 */
public class ViewConfigurationAction extends ConfluenceActionSupport
{
    private final BannerStorage bannerStorage;
    private final LabelManager labelManager;

    /**
     * This is just a helper class to make it easier to reference important
     * information from velocity templates.
     */
    public static class BannerView
    {
        private final Banner banner;
        private final Label label;

        BannerView(Banner banner, Label label)
        {
            this.banner = banner;
            this.label = label;
        }

        public Banner getBanner()
        {
            return banner;
        }

        public Label getLabel()
        {
            return label;
        }
    }

    /**
     * Constructor called by Confluence before the action _is first invoked_.
     * Note that this is different to some other plugins modules which are
     * constructed as soon as the plugin is installed.
     * @param bannerStorage A reference to our {@link BannerStorage} component
     *                      in this plugin. Confluence automatically locates it
     *                      and provides it as a constructor parameter here.
     * @param labelManager  A reference to the Confuence {@link LabelManager}
     *                      component.
     */
    public ViewConfigurationAction(BannerStorage bannerStorage, LabelManager labelManager)
    {
        this.bannerStorage = bannerStorage;
        this.labelManager = labelManager;
    }

    /**
     * This getter method allows the velocity template to access the banner
     * configuration objects stored in the database. A velocity template can
     * access the method by calling '$action.banners'.
     *
     * @return The collection of all configured banners in the plugin, wrapped
     * in a helper object that also provides a reference to the full {@link Label}
     * object that the Banner relates to.
     */
    public Collection<BannerView> getBanners()
    {
        return Collections2.transform(bannerStorage.getConfiguredBanners(), new Function<Banner, BannerView>()
        {
            @Override
            public BannerView apply(Banner banner)
            {
                // Get the label associated with the banner
                Label label = labelManager.getLabel(banner.getLabelId());
                // Store them together in a helper object.
                return new BannerView(banner, label);
            }
        });
    }

    /**
     * The {@link #execute()} method is the main entry point for this class.
     * When the user hits the appropriate URL in a browser, the execute method
     * is called. In this method, the action can do whatever it needs to do in
     * order to determine the result of the action.  The value returned from the
     * execute method is then mapped to a corresponding <result> declaration in
     * the atlassian-plugin.xml. The {@link Action} class has a variety of
     * public, static variables for common return values; for example,
     * Action.SUCCESS, Action.ERROR.
     */
    @Override
    public String execute() throws Exception
    {
        // This is a very simple action, we just need to load the banners from
        // the database and return. The loading is done in the getBanners()
        // method, which will be called by the velocity template.
        return Action.SUCCESS;
    }
}
